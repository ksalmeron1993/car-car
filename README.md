# CarCar

Team:

* Michael Maloney - Auto Sales
* Kevin Salmeron - Auto Services

## Getting Started
Listed below are instructions on how to get CARCAR up and running:

* Begin by forking and then cloning the repository link - https://gitlab.com/maloneym16/project-beta-2/
* Once you have successfully cloned and opened the application in your vscode, you will then need to open and run these docker commands:
* `docker volume create beta-data`
* `docker-compose build`
* `docker-compose up`

* You can launch the frontend on http://localhost:3000

## Design

![Design of CarCar App](Diagram.png "diagram")

## Services


## Inventory Microservice:

In our inventory back end, you will find models, RESTful API's and urls. You will find a list and be able to create a manufacturer, youll be able to find a list and create a vehicle model, and you can list and create a specific vehicle with a model and manufacturer.

## Models:
The inventory microservice has 3 models.

* Manufacturer Model - A model that represent the name of a manufacturer.
* Vehicle Model - A model that represents the name of a vehicle model, a picture url for the vehicle model, and the manufacturer of the vehicle.
* Automobile Model - A model that represents the color of a vehicle, the year of a vehicle, the VIN of a specific vehicle, and the associated vehicle model.

Listed below are api endpoints found in the backend:

* List of all vehicle models - **GET** - http://localhost:8100/api/models/
* Form to add a new model -**PUT** - http://localhost:8100/api/models/new

* List of all automobiles - **GET** - http://localhost:8100/api/automobiles/
* Form to create a new automobile - **PUT** - http://localhost:8100/api/automobiles/new/

* List of all manufacturers - **GET** - http://localhost:8100/api/manufacturers/
* Form to create a manufacturer - **PUT** - http://localhost:8100/api/manufacturers/new


## Service Microservice

In our service backend you will find models, RESTful APIs and urls. In the frontend you will find forms and lists that will represent this to the user. Our service backend allows technicians and appointments to be created. They also allow you to see a list of appointments. Lastly you can see the service appointment history by searing the VIN number. You can also find a poller file with helps get data from the inventory backend. This is necessary because our inventory backend is its own microservice. The poller file helps get VIN data and helps create an AUTOMOBILEVO object.

### Models
The service microservice has 3 models:

* Appointment Model - A model that represents the VIN number, customer name, reason for visiting, the date and time of appointment, and the technician assigned to the appointment.
* Technician Model - A model that represents the techician's name and their employee numbers.
* AutoMobileVO Model - A value object related to the automobile model in the inventory backend.

### Port

* The microservice fois open and running on port 8080.

### CRUD

Listed below are api endpoints found in the backend:

* List Appointments - **GET** - http://localhost:8080/api/appointments/
* Get a specific appointment - **GET** - http://localhost:8080/api/appointments/detail/<int:pk>/
* Get a appoint by vin - **GET** - http://localhost:8080/api/appointments/<str:vin>/
* Create Appointment - **POST** - http://localhost:8080/api/appointments/
* Delete an Appointment - **DELETE** - http://localhost:8080/api/appointments/detail/<int:pk>/
* List Technicians - **GET** - http://localhost:8080/api/technicians/
* Get a specific Technician - **GET** - http://localhost:8080/api/technicians/<int:pk>/
* Create Technician - **POST** - http://localhost:8080/api/technicians/
* Delete a specific Technician - **DELETE** - http://localhost:8080/api/technicians/<int:pk>/
* List of all vehicle models - **GET** - http://localhost:8100/api/models/
* Form to add a new model -**PUT** - http://localhost:8100/api/models/new
* List of all automobiles - **GET** - http://localhost:8100/api/automobiles/
* Form to create a new automobile - **PUT** - http://localhost:8100/api/automobiles/new/
* List of all manufacturers - **GET** - http://localhost:8100/api/manufacturers/
* Form to create a manufacturer - **PUT** - http://localhost:8100/api/manufacturers/new

### Sample Data
* Create Technician - example:
```
{
    "name": "Eric Robbins",
    "employee_number": 221
}
```
* Create an appointment - example:
```
{
    "vin": "22156466adfa",
    "customer_name": "Test Name",
    "reason": "flat tire",
    "date": "2022-12-12T12:00:00+00:00",
    "technician": 1,
    "completed": false,
    "vip": false
}
```
## Sales microservice

In our sales backend you will find models, RESTful APIs and urls. In the frontend you will find forms and lists that will represent this to the user. Our service backend allows customers, sales people, and sales records to be created. They also allow you to see a list of sales history for a salesperson.
Lastly, you can see a list of all of the customers, a list of all of the sales people, and a list of all of the sales.  You can also find a poller file that gets data from the inventory backend.  The poller file helps get VIN data and helps create an AUTOMOBILEVO object.

# Models

The sales microservice has 4 models:

* SalesPerson Model - A model that represents a salesperson with their name and employee number.
* PotentialCustomerModel - A model that represents the Potential Customer, with the customers name, address, and phone number.
* SalesRecord Model - A model that represents the sales records, with the final price, the salesperson who made the sale, the customer who purchased the vehicle, and the automobile that was purchased.
* AutoMobileVO Model - A value object related to the automobile model in the inventory backend.

### Port

* The microservice fois open and running on port 8090.

### CRUD

Listed below are api endpoints found in the backend:

* Get list of customers -**GET** - http://localhost:8090/customers/
* Create Customer - **POST** - http://localhost:8090/api/customers/new/
    example:
```
    {
        "id": 1,
        "name": "P. Sherman",
        "address": "42 Wallabye Way, Sydney Australia",
        "phone_number": "2813308004",
    }
```
* Get a specific customer - **GET** - http://localhost:8090/api/customers/detail/<int:pk>/

* Create Sale - **POST** - http://localhost:8090/api/sales/
    example input:
```
    {
        "sales_person": "Mr. Doogus",
        "customer": "P. Sherman",
        "price": "1000000",
        "id": 1,
    }
```
* Get a specfic Sales - **GET** - http://localhost:8090/api/sales/detail/<int:pk>/
* Get list of sales - **GET** - http://localhost:8090/api/sales/

* List of salespeople  - **GET** - http://localhost:8090/api/salespersons/
* Get a specific salesperson - **GET** - http://localhost:8090/api/salespersons/<int:pk>/
* Create salesperson - **POST** - http://localhost:8090/api/salespersons/new/
    example:
```
    {
        "id": 1,
        "name": "Steve Martin",
        "employee_number = "8675309",
    }
 ```
* Delete a specific salesperson - **DELETE** - http://localhost:8090/api/salespersons/<int:pk>/
