from django.db import models
from django.urls import reverse

# Create your models here.
class Technician(models.Model):
    name = models.CharField(max_length=70)
    #must be positive or 0, must be unique to the employer
    employee_number = models.PositiveBigIntegerField(unique=True)

    def get_api_url(self):
        return reverse('api_list_technician', kwargs={"pk": self.id})


class Appointment(models.Model):
    #null=True means 2 possible values for 'no data' NULL or empty string
    vin = models.CharField(max_length=17, null=True)
    vehicle_owner = models.CharField(max_length=100)
    date_time = models.DateTimeField()
    #many to one relationship with tech class
    technician = models.ForeignKey(
        Technician, related_name='appointments', on_delete=models.CASCADE,
    )
    reason = models.TextField(max_length=500)
    finished = models.BooleanField(default=False)

    #reverse functino allows to retrieve url details from urls.py file, can be used in Django to avoid hardcoding of url’s 
    def get_api_url(self):
        return reverse('api_show_appointment', kwargs={"pk":self.id})


class AutoVO(models.Model):
    #vin is unique 
    vin_number = models.CharField(max_length=17, unique=True)
    #
    import_url = models.CharField(max_length=100,unique=True)
    #The __str__ method tells Django what to print when it needs to print out an instance of the any model. Also what will show in your admin panel
    def __str__(self):
        return self.vin_number



