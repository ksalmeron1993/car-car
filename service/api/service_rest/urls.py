from django.urls import path 
from .views import api_list_technicians, api_show_appointment, api_list_appointments, show_technician

#missing autos path  , check with mike 
urlpatterns = [
    path("technicians/", api_list_technicians, name="api_create_new_tech"),
    path("technicians/<int:pk>/", show_technician, name="show_technician"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("appointments/<str:vin>/", api_list_appointments,
         name="list_appointments_by_vin"),
]
