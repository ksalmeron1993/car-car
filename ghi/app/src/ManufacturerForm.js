import { useEffect, useState } from "react";

function ManufacturerForm() {
    const [manufacturerName, setManufacturerName] = useState("");
    const [submitted, setSubmitted] = useState(false);
    const[invalid, setInvalid] = useState(false);
    

	const handleSubmit = async (event) => {
		event.preventDefault();

		const manufacturer_name = manufacturerName;
		const data = { name: manufacturerName};

		const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(manufacturerUrl, fetchConfig);
		if (response.ok) {
			event.target.reset();
            setManufacturerName("");
            setSubmitted(true);
            setInvalid("");
		}else{
            console.error("Error: Invalid Manufacturer Name");
            setInvalid(true);
        }
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1 className="text-center">Create a New Manufacturer</h1>
					<form id="create-manufacturer-form" onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								onChange={(e) => setManufacturerName(e.target.value)}
								placeholder="manufacturerName"
								required
								type="text"
								name="manufacturerName"
								id="manufacturerName"
								className="form-control"
							/>
							<label htmlFor="manufacturerName">Manufacturer Name</label>
						</div>
						<div className="col text-center">
							<button className="btn btn-primary">Create</button>
						</div>
					</form>
					{invalid && (
						<div
							className="alert alert-danger mb-0 p-4 mt-4"
							id="success-message">
							Sorry , try a different manufacturer name. This is an invalid manufacturer name or that name is already
							taken.
						</div>
					)}
					{!invalid && submitted && (
						<div
							className="alert alert-success mb-0 p-4 mt-4"
							id="success-message">
							Successfully added a new manufacturer!
						</div>
					)}
				</div>
			</div>
		</div>
	);
};
export default ManufacturerForm;
