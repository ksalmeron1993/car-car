from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=20, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.PositiveSmallIntegerField(unique=True, null=True)

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=50)
    number = models.CharField(max_length=10)
    address = models.TextField()


class Sale(models.Model):
    price = models.CharField(max_length=200)

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales",
        on_delete=models.CASCADE,
        null=True
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("show_sales", kwargs={"pk": self.pk})

    def __str__(self):
        return f"Car: {self.autombile} | Customer: {self.customer} "
